extern crate zeke_gl;
#[macro_use]
extern crate bitflags;

#[cfg(not(target_arch = "wasm32"))]
extern crate sdl2;
#[cfg(not(target_arch = "wasm32"))]
#[path = "sdl/mod.rs"]
pub mod app;

pub mod event;
pub mod keyboard;

pub use app::{App, EventIter};
pub use event::Event;

pub struct AppConfig {
    /// Set the window title (only visible on desktop clients)
    pub title: String,
    /// Set the size of the application
    pub size: (u32, u32),
    /// Is the app resizable
    pub resizable: bool,
}