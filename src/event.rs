use crate::keyboard::{Keycode, Scancode};

bitflags! {
    pub struct KeyMod: u16 {
        const NO_MOD = 0x0000;
        const LSHIFT = 0x0001;
        const RSHIFT = 0x0002;
        const LCTRL  = 0x0040;
        const RCTRL  = 0x0080;
        const LALT   = 0x0100;
        const RALT   = 0x0200;
        const LMETA  = 0x0400;
        const RMETA  = 0x0800;
        const NUMLOCK  = 0x1000;
        const CAPSLOCK = 0x2000;
        const MODE     = 0x4000;
    }
}

#[derive(Copy, Clone, Debug)]
pub struct MouseMoveEvent {
    /// Current mouse state
    pub state: u32,
    /// X-position of the mouse cursor
    pub x: i32,
    /// Y-position of the mouse cursor
    pub y: i32,
    /// Relative motion in X direction
    pub xrel: i32,
    /// Relative motion in Y direction
    pub yrel: i32,
}

#[derive(Copy, Clone, Debug)]
pub struct MouseButtonEvent {
    /// Which button
    pub button: u8,
    /// True if pressed, false if released
    pub pressed: bool,
    /// Number of clicks (1 = single click, 2 = double-click)
    pub clicks: u8,
    /// X-position of the cursor relative to the window
    pub x: i32,
    /// Y-position of the cursor relative to the window
    pub y: i32,
}

#[derive(Copy, Clone, Debug)]
pub struct MouseWheelEvent {
    /// Horizontal scroll amount
    pub x: i32,
    /// Vertical scroll amount
    pub y: i32,
    /// True if scrolling is inverted
    pub flipped: bool,
}

#[derive(Debug, Copy, Clone)]
pub struct KeyEvent {
    /// Layout-dependent key code
    pub code: Keycode,
    /// Layout-independent key code (scancode)
    pub scan: Scancode,
    /// Modifiers (e.g. ctrl, alt, shift, meta)
    pub modifiers: KeyMod,
    /// If the user holds down a key then it will repeatedly send events
    pub repeat: bool,
    /// True if the key is down, false if it's up.
    pub down: bool,
}

impl KeyEvent {
    pub fn is_ctrl(&self) -> bool {
        self.modifiers.contains(KeyMod::LCTRL | KeyMod::RCTRL) 
            || self.scan == Scancode::LCTRL || self.scan == Scancode::RCTRL
    }

    pub fn is_alt(&self) -> bool {
        self.modifiers.contains(KeyMod::LALT | KeyMod::RALT)
            || self.scan == Scancode::LALT || self.scan == Scancode::RALT
    }

    pub fn is_shift(&self) -> bool {
        self.modifiers.contains(KeyMod::LSHIFT | KeyMod::RSHIFT)
            || self.scan == Scancode::LSHIFT || self.scan == Scancode::RSHIFT
    }

    pub fn is_meta(&self) -> bool {
        self.modifiers.contains(KeyMod::LMETA | KeyMod::RMETA)
            || self.scan == Scancode::LGUI || self.scan == Scancode::RGUI
    }
}

#[derive(Debug, Clone)]
pub enum Event {
    Unknown,
    /// Mouse button pressed or released
    MouseButton(MouseButtonEvent),
    /// Mouse scroll event
    MouseWheel(MouseWheelEvent),
    /// Mouse cursor moved
    MouseMove(MouseMoveEvent),
    /// A key has been pressed or released
    Key(KeyEvent),
    /// Text entered
    TextInput(String),
    /// Window focus gained
    FocusGained,
    /// Window focus lost (e.g. minimized or clicked away) 
    FocusLost,
    /// Window moved to new location
    WindowMoved(i32, i32),
    /// New window size (in pixels)
    WindowResized(u32, u32),
    /// The application has been requested to quit
    Quit,
}
