
/*
Each event is 20 bytes:

| Off. | Size | Description 
|------|------|------------
|    0 |    1 | Type ID, determines event type
|    1 |    1 | misc byte
|    2 |    2 | misc word
|    4 |    4 | misc i32
|    8 |    4 | misc i32
|   12 |    4 | misc f32
|   16 |    4 | misc f32
|------|------|------------
*/

const EVENT_SIZE = 20;
const EVENT_TYPES = {
    KEYDOWN = 1,
    KEYUP   = 2,
    MOUSEDOWN = 3,
    MOUSEUP   = 4,
    MOUSEMOVE = 5,
    RESIZE = 6,
}

/// Global event data
let event_data = {
    size: 0,
    u8: new Uint8Array(EVENT_SIZE * 10),
};
event_data.f32 = new Float32Array(event_data.buffer);
event_data.u32 = new Uint32Array(event_data.buffer);

/**
 * Push an event into the event data buffer.
 * Resize the buffer if it's too small.
 * @param {u8} type_id event type ID
 * @param {u8} b1 event-specific byte
 * @param {u16} w2 event-specific word
 * @param {u32} dw3 event-specific dword
 * @param {u32} dw4 event-specific dword
 * @param {f32} f5 event-specific 32-bit float
 * @param {f32} f6 event-specific 32-bit float
 */
function push_event(type_id, b1, w2, dw3, dw4, f5, f6) {
    let ed = event_data;
    // Resize the buffer if necessary
    if (ed.u8.byteLength == ed.size) {
        let newbuf = new Uint8Array(ed.u8.byteLength * 2);
        newbuf.set(ed.u8);
        ed.u8 = newbuf;
        ed.f32 = new Float32Array(ed.u8);
        ed.u32 = new Int32Array(ed.u8);
    }
    ed.u8[ed.size + 0] = type_id;
    ed.u8[ed.size + 1] = b1;
    ed.u8[ed.size + 2] = w2 & 0xff;
    ed.u8[ed.size + 3] = (w2 >> 8) & 0xff;
    let offset = ed.size / 4;
    ed.u32[offset + 1] = dw3;
    ed.u32[offset + 2] = dw4;
    ed.f32[offset + 3] = f5;
    ed.f32[offset + 4] = f6;
    ed.size += EVENT_SIZE;
}

/// Register various event handlers that catch and store events
function register_events(canvas_id) {
    let canvas = document.getElementById(canvas_id);
    document.body.addEventListener('keydown', (ev) => {
        push_event(EVENT_TYPES.KEYDOWN, 0, ev.keyCode, 0, 0, 0, 0);
    }, true);
    document.body.addEventListener('keyup', (ev) => {
        push_event(EVENT_TYPES.KEYUP, 0, ev.keyCode, 0, 0, 0, 0);
    }, true);
    window.addEventListener('resize', (ev) => {
        push_event(EVENT_TYPES.RESIZE, 0, 0, window.innerWidth, window.innerHeight, 0, 0);
    }, true);
    canvas.addEventListener('mousedown', (ev) => {
        push_event(EVENT_TYPES.MOUSEDOWN, ev.button, 0, ev.clientX, ev.clientY, 0, 0);
    }, true);
    canvas.addEventListener('mouseup', (ev) => {
        push_event(EVENT_TYPES.MOUSEUP, ev.button, 0, ev.clientX, ev.clientY, 0, 0);
    }, true);
    canvas.addEventListener('mousemove', (ev) => {
        push_event(EVENT_TYPES.MOUSEMOVE, 0, 0, ev.clientX, ev.clientY, 0, 0);
    }, true);
}

/// Returns a view into the current array of events, then resets the event buffer
function take_events() {
    let r = new Uint8Array(event_data.u8, 0, event_data.size);
    event_data.size = 0;
    return r;
}