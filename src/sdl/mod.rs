use sdl2;
use sdl2::audio::{AudioSpecDesired, AudioQueue};
use sdl2::video::{GLContext, GLProfile, FullscreenType, SwapInterval, Window};
use std::cell::{Cell, RefCell, Ref};
use std::time::{Duration};
use zeke_gl::WebGLRenderingContext;
use crate::{AppConfig, Event};

mod event;

pub struct App {
    _sdl_context: sdl2::Sdl,
    video: sdl2::VideoSubsystem,
    timer: sdl2::TimerSubsystem,
    audio: sdl2::AudioSubsystem,
    event_pump: sdl2::EventPump,

    audio_device: Option<AudioQueue<i16>>,
    window: Window,
    _sdl_glcontext: GLContext,
    events: RefCell<Vec<Event>>,
    exiting: Cell<bool>,
    /// Minimum frame time
    min_frame_time: u64,
}

impl App {
    pub fn new(config: &AppConfig) -> Result<App, String> {
        let sdl_context = sdl2::init()?;

        println!("SDL2 version: {:?}", sdl2::version::version());

        
        // Tell SDL to prefer the ANGLE/GLESv2 library
        sdl2::hint::set("SDL_OPENGLES_DRIVER", "1");

        let video = sdl_context.video()?;
        let timer = sdl_context.timer()?;
        let audio = sdl_context.audio()?;
        
        // Request the OpenGL profile
        let gla = video.gl_attr();
        gla.set_context_version(3, 0);
        gla.set_context_profile(GLProfile::GLES);
        // gla.set_context_version(3, 3);
        // gla.set_context_profile(GLProfile::Core);

        println!("Loaded");

        // Build the window
        let mut window_builder = video.window(&config.title, config.size.0, config.size.1);
        window_builder.opengl();
        if config.resizable {
            window_builder.resizable();
        }
        let window = window_builder.build()
            .map_err(|e| format!("{:?}", e))?;


        let gl_context = window.gl_create_context()?;

        // Always use vsync for consistency with web platforms and to keep things smooth
        // Maybe allow this to be configurable later, not sure?
        video.gl_set_swap_interval(SwapInterval::VSync)?;

        // Grab the singular, global event pump
        let event_pump = sdl_context.event_pump()?;
        
        Ok(App {
            _sdl_context: sdl_context,
            video,
            timer,
            audio,
            event_pump,

            audio_device: None,
            _sdl_glcontext: gl_context,
            window,
            events: RefCell::new(Vec::new()),
            exiting: Cell::new(false),
            min_frame_time: 0,
        })
    }

    pub fn get_gl_context(&self) -> WebGLRenderingContext {
        WebGLRenderingContext::new(Box::new(
            |s| self.video.gl_get_proc_address(s) as *const std::ffi::c_void
        ))
    }

    /// Set the fullscreen state of the application.
    pub fn set_fullscreen(&mut self, fullscreen: bool) {
        let fst = if fullscreen { FullscreenType::True } else { FullscreenType::Off };
        self.window.set_fullscreen(fst).unwrap();
    }

    /// Tell if the window is currently fullscreen or not.
    pub fn is_fullscreen(&self) -> bool {
        let fst = self.window.fullscreen_state();
        match fst {
            FullscreenType::Off => false,
            _ => true,
        }
    }

    pub fn exit(&self) {
        self.exiting.set(true);
    }

    /// Set the target frames-per-second for the game.
    pub fn set_target_fps(&mut self, fps: f32) {
        if fps == 0. {
            self.min_frame_time = 0;
        } else {
            let ft = 1. / fps as f64;
            self.min_frame_time = (self.timer.performance_frequency() as f64 * ft) as u64;
        }
    }

    /// Get a list of the available audio output devices
    pub fn list_audio_devices(&self) -> Result<Vec<String>, String> {
        let num_devices = self.audio.num_audio_playback_devices()
            .ok_or(String::from("Failed to get list of audio devices"))?;
        let mut devices = Vec::new();
        for i in 0..num_devices {
            let name = self.audio.audio_playback_device_name(i)?;
            devices.push(name);
        }
        Ok(devices)
    }

    pub fn use_audio_device(&mut self, name: Option<&str>) -> Result<(), String> {
        let default_spec = AudioSpecDesired {
            freq: Some(44_100), // 44.1 KHz
            channels: Some(2),  // stereo
            samples: None,      // default
        };
        self.audio_device = Some(self.audio.open_queue(name, &default_spec)?);
        Ok(())
    }

    /// Transform platform-specific events into our generic event objects
    fn handle_events(&mut self) -> bool {
        let mut events_vec = self.events.borrow_mut();
        events_vec.clear();
        for ev in self.event_pump.poll_iter() {
            if let Some(ev2) = event::convert_sdl_event(&ev) {
                events_vec.push(ev2);
            }
        }
        true
    }

    /// Returns an iterator over the current set of events.
    pub fn events(&self) -> EventIter {
        EventIter {
            events: self.events.borrow(),
            index: 0,
        }
    }

    /// Run the update function in a loop.
    /// The function will receive a mutable reference to the App object
    /// and the delta time (in seconds).
    pub fn run<F>(&mut self, mut callback: F)
    where
        F: FnMut(&mut Self, f64) -> ()
    {
        let mut last;
        let mut now = self.timer.performance_counter();
        let mut next_frame;
        loop {
            last = now;
            now = self.timer.performance_counter();
            next_frame = now + self.min_frame_time;
            
            // delta time in seconds
            let delta_time = ((now - last) * 1000 * 1000) as f64
                / self.timer.performance_frequency() as f64;
            
            if !self.handle_events() {
                break
            }
            callback(self, delta_time);
            if self.exiting.get() {
                break;
            }
            self.window.gl_swap_window();

            std::thread::sleep(Duration::from_millis(1));
            while self.timer.performance_counter() < next_frame {
                std::thread::yield_now();
            }
        }
    }
}

pub struct EventIter<'a> {
    events: Ref<'a, Vec<Event>>,
    index: usize,
}

impl<'a> Iterator for EventIter<'a> {
    type Item = Event;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(ev) = self.events.get(self.index) {
            self.index += 1;
            Some(ev.clone())
        } else {
            None
        }
    }
}
