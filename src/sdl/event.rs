use crate::event::*;
use crate::keyboard::*;
use sdl2::event::Event as Ev;
use sdl2::mouse::MouseButton;

/// Convert an SDL2 Event into a zeke-app Event
pub fn convert_sdl_event(ev: &Ev) -> Option<Event> {
    match ev {
        Ev::MouseButtonDown { mouse_btn, x, y, clicks, .. } => {
            Some(Event::MouseButton(convert_mouse_button_event(*mouse_btn, *x, *y, *clicks, true)))
        },
        Ev::MouseButtonUp { mouse_btn, x, y, clicks, .. } => {
            Some(Event::MouseButton(convert_mouse_button_event(*mouse_btn, *x, *y, *clicks, false)))
        },
        Ev::MouseMotion { mousestate, x, y, xrel, yrel, .. } => {
            Some(Event::MouseMove(MouseMoveEvent {
                state: mousestate.to_sdl_state(),
                x: *x,
                y: *y,
                xrel: *xrel,
                yrel: *yrel,
            }))
        },
        Ev::MouseWheel { x, y, direction, .. } => {
            use sdl2::mouse::MouseWheelDirection as MWD;
            Some(Event::MouseWheel(MouseWheelEvent {
                x: *x,
                y: *y,
                flipped: match direction { MWD::Flipped => true, _ => false },
            }))
        },
        Ev::Quit { .. } => {
            Some(Event::Quit)
        },
        Ev::KeyDown { keycode, scancode, keymod, repeat, .. } => {
            Some(Event::Key(convert_key_event(*keycode, *scancode, *keymod, *repeat, true)))
        },
        Ev::KeyUp { keycode, scancode, keymod, repeat, .. } => {
            Some(Event::Key(convert_key_event(*keycode, *scancode, *keymod, *repeat, false)))
        },
        Ev::TextInput { text, .. } => {
            Some(Event::TextInput(text.clone()))
        },
        Ev::Window { win_event, .. } => {
            use sdl2::event::WindowEvent as WEv;
            match win_event {
                WEv::Moved(x,y) => Some(Event::WindowMoved(*x, *y)),
                WEv::Resized(w,h) => Some(Event::WindowResized(*w as u32, *h as u32)),
                WEv::FocusLost => Some(Event::FocusLost),
                WEv::FocusGained => Some(Event::FocusGained),
                _ => None,
            }
        },
        _ => { None },
    }
}

fn convert_mouse_button_event(mouse_btn: MouseButton, x: i32, y: i32, clicks: u8, pressed: bool) -> MouseButtonEvent {
    let button = match mouse_btn {
        MouseButton::Left => 1,
        MouseButton::Middle => 2,
        MouseButton::Right => 3,
        _ => 0,
    };
    MouseButtonEvent {
        button, x, y, clicks, pressed,
    }
}

/// Convert data pulled from an SDL2 key event into a zeke-app KeyEvent object
fn convert_key_event(
    keycode: Option<sdl2::keyboard::Keycode>,
    scancode: Option<sdl2::keyboard::Scancode>,
    keymod: sdl2::keyboard::Mod,
    repeat: bool,
    down: bool,
) -> KeyEvent {
    let code = if let Some(v) = keycode { v as u32 } else { 0 };
    let scan = if let Some(v) = scancode { v as u32 } else { 0 };
    KeyEvent {
        code: Keycode::from_u32(code),
        scan: Scancode::from_u32(scan),
        modifiers: KeyMod::from_bits_truncate(keymod.bits()),
        repeat,
        down,
    }
}
