use std::collections::HashMap;

pub struct AudioSystem {
    
}


struct SoundResource {
    
}

pub struct Sound {

    looping: bool,
    volume: f32,
    rate: f32,
    sprites: HashMap<String, SoundSprite>,
}



struct SoundSprite {
    pub offset: u32,
    pub duration: u32,
    pub looping: bool,
}

